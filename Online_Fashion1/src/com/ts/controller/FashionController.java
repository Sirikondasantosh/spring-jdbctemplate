package com.ts.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ts.dto.Fashion;
import com.ts.service.FashionService;

@WebServlet("/FashionController")
public class FashionController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		if (action == null) {
			try {

				List<Fashion> fashionsList = new FashionService().getFashionsList();
				request.setAttribute("fashionsList", fashionsList);
				request.getRequestDispatcher("home.jsp").forward(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (action.equals("additems")) {
			try {
				List<Fashion> fashionsList = new FashionService().getFashionsList();
				request.setAttribute("fashionsList", fashionsList);
				request.getRequestDispatcher("form.jsp").forward(request, response);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}

		if (action.equals("edititems")) {
			try {
				List<Fashion> fashionsList = new FashionService().getFashionsList();
				request.setAttribute("fashionsList", fashionsList);
				request.getRequestDispatcher("Edit.jsp").forward(request, response);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		if (action.equals("updateitems")) {
			try {
				List<Fashion> fashionsList = new FashionService().getFashionsList();
				request.setAttribute("fashionsList", fashionsList);
				request.getRequestDispatcher("UpdateServlet").forward(request, response);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		if (action.equals("deleteitems")) {
			try {
				List<Fashion> fashionsList = new FashionService().getFashionsList();
				request.setAttribute("fashionsList", fashionsList);
				request.getRequestDispatcher("DeleteServlet").forward(request, response);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if (action.equals("home")) {
			try {
				List<Fashion> fashionsList = new FashionService().getFashionsList();
				request.setAttribute("fashionsList", fashionsList);
				request.getRequestDispatcher("FashionController").forward(request, response);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
