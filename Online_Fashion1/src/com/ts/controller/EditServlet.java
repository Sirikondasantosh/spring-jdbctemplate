package com.ts.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ts.dao.FashionDAO;

import com.ts.dto.Fashion;

@WebServlet("/UpdateServlet")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public EditServlet() {
		super();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<center><h1>Update</h1></Center>");
		int id = Integer.parseInt(request.getParameter("id"));
		Fashion f = null;
		try {
			f = (Fashion) FashionDAO.getFashionList(id);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		out.print("<center><form action ='UpdateServlet' method = 'get'>");

		out.print("<table>");
		out.print("<tr><td></td><td><input type='hidden' name='id' value='" + f.getId() + "'/></td></tr>");
		out.print("<tr><td>apperalName:</td><td><input type='text' name='apperalName' value='" + f.getApparelName()
				+ "'/></td></tr>");
		out.print("<tr><td>type:</td><td><input type='text' name='type' value='" + f.getType() + "'/></td></tr>");
		out.print("<tr><td>size:</td><td><input type='text' name='size' value='" + f.getSize() + "'/></td></tr>");
		out.print("<tr><td>gender:</td><td><input type='text' name='gender' value='" + f.getGender() + "'/></td></tr>");
		out.print("<tr><td>price:</td><td><input type='text' name='price' value='" + f.getPrice() + "'/></td></tr>");
		out.print("<tr><td colspan='2'> <Button type='submit' value='Save1'/>SAVE</td></tr>");
		out.print("</table>");
		out.print("</form></center>");

		out.close();
	}

}
