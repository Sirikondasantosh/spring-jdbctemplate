package com.ts.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ts.dao.FashionDAO;
import com.ts.dto.Fashion;


@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String sid = request.getParameter("id");
		int id = Integer.parseInt(sid);

		String apperalName = request.getParameter("apperalName");
		String type = request.getParameter("type");
		String size = request.getParameter("size");
		String gender = request.getParameter("gender");
		int price = Integer.parseInt(request.getParameter("price"));

		Fashion b = new Fashion();
		b.setApparelName(request.getParameter("apparelName"));
		b.setType(request.getParameter("type"));
		b.setSize(request.getParameter("size"));
		b.setGender(request.getParameter("gender"));
		b.setPrice(price);

		int status = FashionDAO.update(b);
		if (status > 0) {
			out.print("<p>Items saved successfully!</p>");
			request.getRequestDispatcher("FashionController").include(request, response);
		} else {
			out.println("Sorry! try again.....");
		}

		out.close();

	}

}
