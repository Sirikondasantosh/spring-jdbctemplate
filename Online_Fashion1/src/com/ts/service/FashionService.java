package com.ts.service;

import java.util.List;

import com.ts.dao.FashionDAO;
import com.ts.dto.Fashion;

public class FashionService
{
   public List<Fashion> getFashionsList()
   {
	  return new FashionDAO().getList();
   }
}
