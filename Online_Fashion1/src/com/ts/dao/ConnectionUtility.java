package com.ts.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtility
{
	public static Connection getConnection()  {
		Connection connection = null;
		try {
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/fashion", "root", "root");			
		} catch (ClassNotFoundException e) {
			System.out.println(" DAOUtility - getConnection() : "+e.toString());
			
		} catch (SQLException e) {
			System.out.println(" DAOUtility - getConnection() : "+e.toString());
			
		}
		return connection;
	}

	public static void close(Connection con)  {
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
}
