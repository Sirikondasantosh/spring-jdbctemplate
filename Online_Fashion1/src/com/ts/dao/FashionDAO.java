package com.ts.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

import com.mysql.jdbc.Statement;
import com.ts.dto.Fashion;

public class FashionDAO {
	public static int save(Fashion f) {
		int status = 0;
		PreparedStatement ps = null;
		try {
			Connection con = (Connection) ConnectionUtility.getConnection();
			ps = con.prepareStatement("insert into tbl_fashion(apparelName,type,size,gender,price) values (?,?,?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, f.getApparelName());
			ps.setString(2, f.getType());
			ps.setString(3, f.getSize());
			ps.setString(4, f.getGender());
			ps.setInt(5, f.getPrice());

			status = ps.executeUpdate();

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return status;
	}
	
	public static int save1(Fashion f) {
		int status = 0;
		PreparedStatement ps = null;
		try {
			Connection con = (Connection) ConnectionUtility.getConnection();
			ps = con.prepareStatement("insert into tbl_fashion(apparelName,type,size,gender,price) values (?,?,?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, f.getApparelName());
			ps.setString(2, f.getType());
			ps.setString(3, f.getSize());
			ps.setString(4, f.getGender());
			ps.setInt(5, f.getPrice());

			status = ps.executeUpdate();

			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return status;
	}

	public static int update(Fashion f) {
		int status = 0;
		PreparedStatement ps = null;
		try {
			Connection con = (Connection) ConnectionUtility.getConnection();
			ps = con.prepareStatement("update tbl_fashion set apparelName=?,type=?,size=?,gender=?,price=? where id=?",
					PreparedStatement.RETURN_GENERATED_KEYS);
		
			ps.setString(1, f.getApparelName());
			ps.setString(2, f.getType());
			ps.setString(3, f.getSize());
			ps.setString(4, f.getGender());
			ps.setInt(5, f.getPrice());
			ps.setInt(6, f.getId());

			status = ps.executeUpdate();
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return status;
	}
	
	public static int delete(int id) {
		int status=0;
		PreparedStatement ps=null;
		try {
			Connection con=(Connection) ConnectionUtility.getConnection();
			 ps=con.prepareStatement("delete from tbl_fashion where id=?");
		ps.setInt(1, id);
		status=ps.executeUpdate();
		con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public List<Fashion> getList() {
		Connection con = (Connection) ConnectionUtility.getConnection();
		try {
			PreparedStatement pst = con.prepareStatement("select *from tbl_fashion");
			ResultSet resultSet = (ResultSet) pst.executeQuery();
			if (resultSet.next()) {
				List<Fashion> fashionList = new ArrayList<Fashion>();
				do {
					Fashion fashion = new Fashion();
					fashion.setId(resultSet.getInt(1));
					fashion.setApparelName(resultSet.getString(2));
					fashion.setType(resultSet.getString(3));
					fashion.setSize(resultSet.getString(4));
					fashion.setGender(resultSet.getString(5));
					fashion.setPrice(resultSet.getInt(6));
					fashionList.add(fashion);
				} while (resultSet.next());

				return fashionList;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			ConnectionUtility.close(con);
		}

		return null;
	}
	
	
	public static List<Fashion> getFashionList(int id) throws ClassNotFoundException, SQLException {
		List<Fashion> list = new ArrayList<Fashion>();
		Connection con = (Connection) ConnectionUtility.getConnection();
		Statement stmt = (Statement) con.createStatement();
		ResultSet rs = (ResultSet) stmt.executeQuery("select * from tbl_fashion");
		Fashion f ;
		while (rs.next()) {
			f = new Fashion();
			f.setId(rs.getInt(1));
			f.setApparelName(rs.getString(2));
			f.setType(rs.getString(3));
			f.setSize(rs.getString(4));
			f.setGender(rs.getString(5));
			f.setPrice(rs.getInt(6));
			list.add(f);

		}

		return list;

	}
}
