package fashion.Dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

import fashion.dto.fashion;

public class fashionDao {

	public static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/fashion1", "root", "root");
		} catch (Exception e) {
			System.out.println(e);
		}
		return con;
	}

	public static int save(fashion f) {
		int status = 0;
		try {
			Connection con = fashionDao.getConnection();
			PreparedStatement ps = con
					.prepareStatement("insert into fashion1(apparelName,type,size,gender,price) values(?,?,?,?,?)");
			ps.setString(1, f.getApparelName());
			ps.setString(2, f.getType());
			ps.setString(3, f.getSize());
			ps.setString(4, f.getGender());
			ps.setInt(5, f.getPrice());

			status = ps.executeUpdate();

			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static int update(fashion f) {
		int status = 0;
		int id = 0;
		ResultSet rs = null;
		try {
			Connection con = fashionDao.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"update fashion1 set apparelName=?,type=?,size=?,gender=?,price=? where id=?",
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, f.getApparelName());
			ps.setString(2, f.getType());
			ps.setString(3, f.getSize());
			ps.setString(4, f.getGender());
			ps.setInt(5, f.getPrice());
			ps.setInt(6, f.getId());

			status = ps.executeUpdate();
			rs = ps.getGeneratedKeys();

			rs.next();

			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();

		}
		return status;
	}

	public static int delete(int id) {
		int status = 0;

		try {
			Connection con = fashionDao.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from fashion1 where id=?");
			ps.setInt(1, id);

			status = ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static fashion getfashionById(int id) {
		fashion f = new fashion();

		try {
			Connection con = fashionDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select*from fashion1 where id=?");
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				f.setId(rs.getInt(1));
				f.setApparelName(rs.getString(2));
				f.setType(rs.getString(3));
				f.setSize(rs.getString(4));
				f.setGender(rs.getString(5));
				f.setPrice(rs.getInt(6));

			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return f;

	}

	public static List<fashion> getallfashiondetails(int id) {
		List<fashion> list = new ArrayList<fashion>();
		try {
			Connection con = fashionDao.getConnection();
			PreparedStatement ps = con.prepareStatement("select*from fashion1");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				fashion f = new fashion();
				f.setId(rs.getInt(1));
				f.setApparelName(rs.getString(2));
				f.setType(rs.getString(3));
				f.setSize(rs.getString(4));
				f.setGender(rs.getString(5));
				f.setPrice(rs.getInt(6));
				list.add(f);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
