package fashion.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import fashion.Dao.fashionDao;
import fashion.dto.fashion;

/**
 * Servlet implementation class SaveServlet
 */
@WebServlet("/SaveServlet")
public class SaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public SaveServlet() {
        super();
    }

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		  String apparelName=request.getParameter("apparelName"); 
	        String type=request.getParameter("type");
	        String size=request.getParameter("size");
	        String gender=request.getParameter("gender");
	        int price=Integer.parseInt(request.getParameter("price"));
	        
	          
	     fashion b=new fashion();  
	      b.setApparelName(apparelName);
	      b.setType(type);
	      b.setSize(size);
	      b.setGender(gender); 
	      b.setPrice(price);
	      
	     
	          
	        int status=fashionDao.save(b);  
	        if(status>0){  
	           out.print("<p>Items saved successfully!</p>");  
	            request.getRequestDispatcher("index.jsp").include(request, response);  
	        }else{  
	            out.println("Sorry! try again.....");  
	        }  
	          
	        out.close();  
	    }  
		
	}


