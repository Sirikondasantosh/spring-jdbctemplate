package fashion.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fashion.Dao.fashionDao;
import fashion.dto.fashion;

/**
 * Servlet implementation class ViewServlet
 */
@WebServlet("/ViewServlet")
public class ViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		  response.setContentType("text/html");  
	        PrintWriter out=response.getWriter();  
	        out.println("<a href='index.html'>Add New fashion</a>");  
	        out.println("<h1>fashion List</h1>");  
	        
	        List<fashion>list=fashionDao.getallfashiondetails(0);
	        out.print("<table border='1' width='100%'");
	        
	        out.print("<tr><th>Id</th><th>apparelName</th><th>type</th><th>size</th><th>gender</th><th>price</th><th>Edit</th><th>Delete</th></tr>");  

	        for(fashion f:list) {
	        	
out.print("<tr><td>"+f.getId()+"</td><td>"+f.getApparelName()+"</td><td>"+f.getType()+"</td> <td>"+f.getSize()+"</td><td>"+f.getGender()+"</td><td>"+f.getPrice()+"</td><td><a href='EditServlet?id="+f.getId()+"'>edit</a></td>  <td><a href='deleteServlet?id="+f.getId()+"'>delete</a></td></tr>");
	        }
	        out.print("</table>");
	        
	        out.close();
	}

	

}
