package fashion.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fashion.Dao.fashionDao;
import fashion.dto.fashion;

/**
 * Servlet implementation class EditServlet2
 */
@WebServlet("/EditServlet2")
public class EditServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditServlet2() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			//String sid = request.getParameter("id");
			//int id = Integer.parseInt(sid);
			

			String apparelName = request.getParameter("apparelName");
			String type = request.getParameter("type");
			String size = request.getParameter("size");
			String gender = request.getParameter("gender");
			int price = Integer.parseInt(request.getParameter("price"));

			fashion b = new fashion();
			b.setApparelName(apparelName);
			b.setType(type);
			b.setSize(size);
			b.setGender(gender);
			b.setPrice(price);

			int status = fashionDao.update(b);
			if (status > 0) {
				out.print("<p>Items Updated successfully!</p>");
				request.getRequestDispatcher("ViewServlet").include(request, response);
			} else {
				out.println("Sorry! try again.....");
			}

			out.close();
		}
	}

}
