package fashion.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fashion.Dao.fashionDao;
import fashion.dto.fashion;

@WebServlet("/EditServlet")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public EditServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<h1> Update fashion</h1>");
		String sid = request.getParameter("id");
		int id = Integer.parseInt(sid);
		fashion f = fashionDao.getfashionById(id);

		out.print("<form action='EditServlet2' method='post'>");
		out.print("<table>");

	//	out.print("<tr><td></td><td><input type='hidden' name='id' value='" + f.getId() + "'/></td></tr>");
		out.print("<tr><td>apparelName:</td><td><input type='text' name='apparelName' value='" + f.getApparelName()
				+ "'/></td></tr>");
		out.print("<tr><td>type:</td><td><input type='type' name='type' value='" + f.getType() + "'/>  </td></tr>");
		out.print("<tr><td>Size:</td><td><input type='size' name='size' value='" + f.getSize() + "'/></td></tr>");
		out.print("<tr><td>Gender:</td><td><input type='gender' name='gender' value='" + f.getGender()
				+ "'/>  </td></tr>");
		out.print("<tr><td>Price:</td><td><input type='price' name='price' value='" + f.getPrice() + "'/>  </td></tr>");
		out.print("</td></tr>");

		out.print("<tr><td colspan='2'><input type='submit' value='Edit & Save '/></td></tr>");

		out.print("</table>");
		out.print("</form>");

		out.close();
	}

}
